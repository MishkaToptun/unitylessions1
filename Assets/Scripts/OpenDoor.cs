﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    Vector3 nextPosition;
    [SerializeField]
    float openSpeed = 1F;
    [SerializeField]
    float waitTime = 1F;
    bool open = false;
  public void Open()
    {
        StartCoroutine(OpenDoorAnim());
    }
  IEnumerator OpenDoorAnim()
    {
       
        yield return new WaitForSeconds(waitTime);
        nextPosition = transform.position - new Vector3(0, 2, 0);
        open = true;

    }

    void Update()
    {
        if (open)
         transform.position = Vector3.Lerp(transform.position, nextPosition, openSpeed * Time.deltaTime);
    }
}
