﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBonus : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    int lifeBonus = 10;
    [SerializeField]
    float destroyTime = 2F;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<HealthStatus>().AddHealh(lifeBonus);
            Destroy(this);
            Destroy(this.gameObject, destroyTime);
        }
    }
}
