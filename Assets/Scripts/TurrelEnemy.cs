﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurrelEnemy : MonoBehaviour
{
    [SerializeField]
    GameObject rocketPrefab;
    //[SerializeField]
    public Transform patronSpawnPoint;
    [SerializeField]
    float attackInterval = 3;
    [SerializeField]
    float attackStrenght = 10F;
    GameObject zielObject;
    float startTime = 0;
    bool attack = true;
    void Update()
    {
       
        if (zielObject != null)
        {
            this.transform.LookAt(zielObject.transform);

            //StartCoroutine(TurrelAttack(attackInterval));
            if (attack)
            {
                attack = false;
                StartCoroutine(TurrelAttack(attackInterval));
            }
        }
    }
    IEnumerator TurrelAttack(float Time)
    {
        yield return new WaitForSeconds(Time);
        Attack();
        attack = true;
    }
    private void Attack()
    {
        var obj = Instantiate(rocketPrefab, patronSpawnPoint.position, patronSpawnPoint.parent.rotation);
        float alpha = this.transform.rotation.eulerAngles.y;
        
        obj.GetComponent<Rigidbody>().AddForce(attackStrenght * Mathf.Sin(Mathf.Deg2Rad*alpha), 0, attackStrenght * Mathf.Cos(Mathf.Deg2Rad * alpha));
    }

    void OnTriggerEnter(Collider collider)
    {

    }
    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            zielObject = collider.gameObject;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            zielObject = null;
        }
    }

}
