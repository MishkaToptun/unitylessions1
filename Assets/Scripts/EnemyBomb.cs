﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBomb : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    int destroyTime = 5;
    void Start()
    {
        Destroy(this.gameObject, destroyTime);
    }
    
    
}
