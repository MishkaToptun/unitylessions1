﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyBlockTrigger : MonoBehaviour
{
    [SerializeField]
    UnityEvent putKeyEvent;
    [SerializeField]
    string keyTag = "Player";
    [SerializeField]
    GameObject keyObject;
    [SerializeField]
    bool rotate = true;
    void FixedUpdate()
    {
        if (rotate)
        transform.Rotate(0, 1, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        //Срабатывает на заданный тег или на заданный обьект
        if (other.gameObject.tag == keyTag | other.gameObject==keyObject)
        {
            putKeyEvent?.Invoke();
            Destroy(this.gameObject, 1);
        }
    }

}
