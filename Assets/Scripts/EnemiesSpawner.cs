﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Transform[] spawnerPoints;
    [SerializeField]
    GameObject enemy;
    [SerializeField]
    int timeInterval = 5;
    [SerializeField]
    int enemyCount = 10;
    int curEnemyCount = 0;
    float startTime;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (startTime==0)
        startTime = Time.time;
        if (Time.time - startTime > timeInterval)
        {
            if (curEnemyCount < enemyCount)
            {
                CreateEnemy();
                startTime = Time.time;
            }
        }
    }
    void CreateEnemy()
    {
        int index=Random.Range(0, spawnerPoints.Length);

        var localEnemy = Instantiate(enemy,spawnerPoints[index].position,Quaternion.Euler(0,0,0));
        curEnemyCount++;
    }
}
