﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    
    NavMeshAgent agent;
    [SerializeField]
    Transform player;
    Vector3 startPosition;
    bool playerVisible = false;
    bool baseStatus = true;
    [SerializeField]
    Animator animator;
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        startPosition = transform.position;
        //player = GameObject.FindWithTag("Player").transform;
        animator = this.GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            baseStatus = false;
            player = other.transform;
            animator.SetBool("Move", true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = null;
            StartCoroutine(ReturnBaseStatus(3F));
        }
    }

    // Update is called once per frame
    void Update()
    {
       if (player != null)
        {
            agent.SetDestination(player.position);
        }
        else
        {
            if (baseStatus)
            agent.SetDestination(startPosition);
            if ((transform.position - startPosition).magnitude < 0.1F)
            {
                animator.SetBool("Move", false);
            }
        }

    }
    IEnumerator ReturnBaseStatus(float time)
    {
        yield return new WaitForSeconds(time);
        baseStatus = true;
    }
}
