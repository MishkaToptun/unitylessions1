﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthStatus : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    int health=1;
    [SerializeField]

    TextMeshProUGUI lifeText;
    void Start()
    {
        if (lifeText != null)
        {
            lifeText.text = health.ToString();
        }
    }
    public void AddHealh(int add)
    {
        health += add;
        if (lifeText != null)
        {
            lifeText.text = health.ToString();
        }
    }
    void FixedUpdate()
    {
        if (health<=0)
        {
            Destroy(this.gameObject);
        }    
    }
    
}
