﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody thisBody;
    [SerializeField] 
    float moveSpeed=1F;
    [SerializeField]
    float jumpForce = 3F;
    [SerializeField]
    bool groundContact = false;
    ForceMode forceModeMove = ForceMode.Force;
    [SerializeField]
    Transform playerAvatar;
    [SerializeField]
    GameObject patrone;
    [SerializeField]
    Transform patronStartPos;
    [SerializeField]
    float attackStrength = 3F;
    [SerializeField]
    Animator playerAnimator;
    PlayerAnimationState _animationState;
    PlayerAnimationState playerStatus
    {
        get { return _animationState; }
        set
        {
            _animationState = value;
            if (_animationState == PlayerAnimationState.Move)
            {
                playerAnimator.SetBool("Move", true);
                playerAnimator.SetBool("Stay", false);
                playerAnimator.SetBool("Attack", false);

            }
            if (_animationState == PlayerAnimationState.Attack)
            {
                playerAnimator.SetBool("Attack", true);
                playerAnimator.SetBool("Move", false);
                playerAnimator.SetBool("Stay", false);


            }
            if (_animationState == PlayerAnimationState.Stay)
            {
                playerAnimator.SetBool("Stay", true);
                playerAnimator.SetBool("Move", false);
                playerAnimator.SetBool("Attack", false);

            }
        }
    }
    void Start()
    {
        thisBody = this.GetComponent<Rigidbody>();
        playerStatus = PlayerAnimationState.Stay;
    }

    // Update is called once per frame
    void Update()
    {
       
        MovePlayer();
        if (Input.GetMouseButtonDown(1))
        {
            playerStatus = PlayerAnimationState.Attack;
            Attack();
        }

    }
    void MovePlayer()
    {
        playerStatus = PlayerAnimationState.Stay;
        if (Input.GetKey(KeyCode.W))
        {
            playerAvatar.rotation = Quaternion.Euler(0, 0, 0);
            thisBody.MovePosition(new Vector3(transform.position.x, transform.position.y, transform.position.z+moveSpeed));
            playerStatus = PlayerAnimationState.Move;
        }
        if (Input.GetKey(KeyCode.S))
        {
            thisBody.MovePosition(new Vector3(transform.position.x, transform.position.y, transform.position.z -moveSpeed));
            playerAvatar.rotation = Quaternion.Euler(0, 180,0);
            playerStatus = PlayerAnimationState.Move;
        }
        if (Input.GetKey(KeyCode.A))
        {
            thisBody.MovePosition(new Vector3(transform.position.x-moveSpeed, transform.position.y, transform.position.z));
            playerAvatar.rotation = Quaternion.Euler(0, 270, 0);
            playerStatus = PlayerAnimationState.Move;
        }
        if (Input.GetKey(KeyCode.D))
        {
            thisBody.MovePosition(new Vector3(transform.position.x+moveSpeed, transform.position.y, transform.position.z ));
            playerAvatar.rotation = Quaternion.Euler(0, 90, 0);
            playerStatus = PlayerAnimationState.Move;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
           if (groundContact == true)
            {
                thisBody.AddForce(new Vector3(0, 1, 0) * jumpForce, ForceMode.Impulse);
            }

        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            groundContact = true;
        }

    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            groundContact = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            groundContact = false;
        }
    }
    void Attack()
    {
        GameObject p = Instantiate(patrone, patronStartPos.position, patronStartPos.rotation);
        var rb=p.GetComponent<Rigidbody>();
        //rb.useGravity = false;
        
        float alpha = patronStartPos.rotation.eulerAngles.y;
        Vector3 attackVector = new Vector3(attackStrength * Mathf.Sin(Mathf.Deg2Rad * alpha), 0, attackStrength * Mathf.Cos(Mathf.Deg2Rad * alpha));
        rb.AddForce(attackVector,ForceMode.VelocityChange);
    }
    enum PlayerAnimationState
    {
        Stay,Attack,Move,Jump
    }

}
