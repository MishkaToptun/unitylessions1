﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Transform posA;
    [SerializeField]
    Transform posB;
    [SerializeField]
    float Speed=0.3F;
    Vector3 nextPos;
    void Start()
    {
        nextPos = posA.position;
    }
    // Update is called once per frame
    void Update()
    {
    if (transform.position != nextPos)
        {
            transform.position = Vector3.Lerp(transform.position, nextPos, Speed * Time.deltaTime);
        }
    if ((transform.position - posA.position).magnitude<=0.2F)
        {
            nextPos = posB.position;
        }
    if ((transform.position - posB.position).magnitude<=0.2F)
        {
            nextPos = posA.position;
        }
    }
    void OnCollisionEnter(Collision col)
    {
        col.gameObject.transform.parent = transform;
    }
}
